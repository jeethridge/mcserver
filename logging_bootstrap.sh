#!/bin/bash
set -e

#set up and configure rsyslog
cp /vagrant/50-default.conf /etc/rsyslog.d/50-default.conf
cp /vagrant/minecraft.conf /etc/rsyslog.d/00-minecraft.conf
service rsyslog restart
