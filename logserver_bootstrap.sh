#!/bin/bash
set -e

#Set up and configure rsyslog server
cp /vagrant/rsyslog.conf /etc/rsyslog.conf
sudo service rsyslog restart
