Minecraft Server Application Service Infrastructure
===================================================

Problem Statement
-----------------
DevOps Challenge

Let's set up a Minecraft server. For this challenge,
you'll need to set up a Minecraft server as a production ready application.
You'll want to consider a few things for this:

How will you know when you are experiencing a failure?
How will you diagnose failure?
How will you recover from failure?
How will you scale?

Take some time to set up an MVP of the architecture you would use.
Don't spend to much time on any one thing, just do enough to demonstrate your
knowledge within the domain and so that we can have a discussion about your
design choices. Submitting the challenge is more important than completing it.


Approach
--------

Requirements
~~~~~~~~~~~~
Based on the challenge I have identified the following areas of concern which
I believe should be addressed by the solution:

1. Deployment
2. Fault Detection
3. Fault Diagnosis
4. Fault Recovery
5. Scalability of items 1-4

General Solution Outline
~~~~~~~~~~~~~~~~~~~~~~~~~
I have identified the following solutions which may be required to satisfy
all of the needs above.

1. A configuration management tool for automating deployment.
This addesses concerns 1 and 5

2. A log monitoring service which can be configured to detect log errors and
anomalies. This addresses concern 2 and 3.

3. A distributed logging service which can aggregate application server logs
to a unified monitoring/control service. This addresses concerns 2 and 5.

4. Some means of triggering a recovery protocol based on the output of the
log monitoring system. This will address concern 4.


Prioritization
~~~~~~~~~~~~~~
This secsion discusses the prioritization of the above elements in order to
identify the MVP solution.

1. The application must run and be deployable to the production environment.
  * Without this there is no deliverable to the customer.

2. Installation and configuration of the application must be automated.
  * If observing the CAMS values, it follows that since 1 is the highest priority,
    it is also the most important thing to automate first. This reduces the labor
    and probability of error when provisioning a given individual server.

3. Fault Detection
  * Without automatic detection of faults, it is not possible to diagnose or
    recover from faults efficiently. It should be easy to detect errors on a
    given application server. Determining whether there are errors and where they
    are will become the bottleneck in providing reliable service to the customer.

4. Installation and configuration of the fault detection solution must be automated.
  * This follows the the same line of reasoning as item two.

5. Centralized Fault Detection / Logging
  * This is the first step towards scalability. With the creation of an individual
    sever and the ability to detect a failure in place, we are able to manually
    replicate the server deployment across multiple instances. At this stage,
    failure detection across multiple instances becomes the bottleneck in maintenance.
    Hence, centralized monitoring is the next most important step.
    We must be able to know that there IS a fault, and where the fault lies.
    With this information we can find, diagnose, and remediate the situation
    without having to touch every instance.

6. Automatic Fault Diagnosis
  * With centralized monitoring in place, the bottleneck is now the manual
    diagnosis of the fault. If we are able to diagnose the fault automatically
    prior to manual intervention, we may move immediately to remediation of
    issues without having to manually retrace the diagnostic steps each time.

7. Automatic Fault Recovery
  * At this point, the remediation of faults becomes the maitenance bottleneck.

8. Automation of larger scale deployments and configurations of new instances
  * Finally, with automated provisioning of single instances and a scalable
    error detection and recovery solution in place, the bottleneck becomes
    the provisioning of new instances and their configuration. This is
    considered as the lowest priority for the reason that a system that
    is allowed to grow rapidly without the appropriate feedback mechanisms in
    place to detect and remediate failures is expected to become inherently
    unstable.


MVP Solution
------------

Virtualization Platform: Vagrant
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Since this is for demonstration purposes the initial solution will be developed
and tested using Vagrant. This is done simply for the reason that it is a
technology I am familiar with and gives me a fast feedback loop on what works
and what doesn't.

Server OS - Trusty64:
~~~~~~~~~~~~~~~~~~~~~
In the absence of any other requirements, this is a platform that I'm relatively
comfortable dealing with.

Application Server Provisioning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The minecraft application server shall be provisioned through the use of a
simple shell script which collectes the required resources and installs the
minecraft service on the target server.

Logging
~~~~~~~
Centralized logging is handled by rsyslog. Rsyslog was chosen because it was
the simplest approach I could come up with to aggregate system and application
logs on one log server. Periodicallt copying the logfiles from the application
servers on to the logging server seemed like roughly the same amount of effort.

Monitoring
~~~~~~~~~~
Monitoring is incomplete, but a mechanism should be put in place to parse
through the log files and notify staff when errors are detected or if the
server is not running. At a minimum, this would allow staff to be notified of
an error condition or outage within a relatively short period of time.
Diagnosis and remediation steps would still be manual for the MVP.


Retrospective and Future Work
------------------------------

I started off slightly on the wrong foot by wasting a little time looking
at ansible and a few other technologies that would probably be better in
the long haul, but was not capable of integrating into a solution quickly
enough to produce and MVP. While not a huge waste in terms of effort,
this could have been avoided by giving more immediate attention to minimum
requirements.

Things I like about this
~~~~~~~~~~~~~~~~~~~~~~~~

* The whole minecraft install was automated, which I feel was the most
  important thing to nail down first.

* I didn't need to install any additional software to enable pushing remote
  logs to a logging server. Rsynlog was straightforward enough to get set up
  relatively quickly, but seems flexible enough to support a decent centralized
  logging framework.

* The basic logging server setup and configuration is pretty simple, and it
  supports the addition of more application servers without a change in config.

Things I'd like to improve
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* I didn't spend any time on securing anything.

*  The log monitoring isn't there. In retrospect, it would have been better
   to cover this first, and worry about getting the log messages to a log
   server second. I let myself get the two priorities switched around becuase
   the log forwarding represented the larger technical risk to me at the time.
   A simple approach might be to periodically call a script that read the logs
   looking for error messages and then sent out an email notification. I thought perhaps using the
   severity level filtering rsyslog might also help simplify this solution.
   Another crucial thing to have in place is something that tells you if
   a server that should be running simply isn't. The logging doesn't explicitly
   help with that issue.
   I made note of some more advanced monitoring solutions but they did not
   appear to be feasible for me to implement within the timeframe of the excercise.

* I'd like a more flexible and scalable way to configure and deploy the servers.
  Ansible seemed like it might be a good solution to look at for this. The
  stateless nature and lower learning curve seemed like it would create fewer
  headaches from somebody who isn't extremely well versed with the nuances of
  CM tools.

* I'd like to be able to add more redundancy to logging. In particular it would
  be great if logging followed a pubsub pattern so that the log stream so that
  producers and consumers of log data didn't need to explicitly know about
  eachother.

* I'd like to be able to automate the deployment of this infrastructure to
  a publicly acessible cloud services platform like AWS, Digital Ocean, etc.
  I feel like this is an important area to cover if demonstrating a soup-to-nuts
  production-ready application deployment.





Appendix
--------

Resources

1. https://github.com/buddhafinger/vgt-spigot-mc/blob/master/Vagrantfile
#. https://github.com/Ahtenus/minecraft-init/blob/master/config.example
#. https://www.vultr.com/docs/how-to-install-a-minecraft-server-on-ubuntu
#. http://jasonwilder.com/blog/2012/01/03/centralized-logging/
#. http://www.rsyslog.com/
#. https://www.digitalocean.com/community/tutorials/how-to-centralize-logs-with-rsyslog-logstash-and-elasticsearch-on-ubuntu-14-04
#. http://serverfault.com/questions/396136/how-to-forward-specific-log-file-outside-of-var-log-with-rsyslog-to-remote-serv
